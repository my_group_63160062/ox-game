/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev;

/**
 *
 * @author Acer
 */
public class OXBrain {

    private char[][] table;

    public OXBrain(char[][] table) {

        this.table = table;

    }

    public boolean checkIsWin(char S) {
        if (table[0][0] == S && table[0][1] == S && table[0][2] == S) {
            return true;
        }
        if (table[1][0] == S && table[1][1] == S && table[1][2] == S) {
            return true;
        }
        if (table[2][0] == S && table[2][1] == S && table[2][2] == S) {
            return true;
        }
        if (table[0][0] == S && table[1][0] == S && table[2][0] == S) {
            return true;
        }
        if (table[0][1] == S && table[1][1] == S && table[2][1] == S) {
            return true;
        }
        if (table[0][2] == S && table[1][2] == S && table[2][2] == S) {
            return true;
        }
        if (table[0][0] == S && table[1][1] == S && table[2][2] == S) {
            return true;
        }
        if (table[0][2] == S && table[1][1] == S && table[2][0] == S) {
            return true;
        }

        return false;

    }

    public char getOX(int x, int y) {
        return table[x][y];
    }

    public void setTable(int x, int y, char s) {
        table[x][y] = s;

    }

    public void callTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }

    }

}
