/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OX {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        OXBrain OXGame = new OXBrain(new char[3][3]);
        int x_pos, y_pos;
        char winner = 0;
        System.out.println("Welcome To OX Game");
        // Set Default ( Empty ) Table. ↓
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                OXGame.setTable(i, j, '-');
            }
        }
        // Call Table For The First Time. ↓
        OXGame.callTable();
        int round = 0;
        // Maximum Round Of OX Game Is 9. ↓
        while (round < 9) {
            char player;
            if (round % 2 != 0) {
                player = 'X';
            } else {
                player = 'O';
            }
            System.out.println("Turn " + player);
            System.out.println("Please input row, col: ");
            x_pos = kb.nextInt();
            y_pos = kb.nextInt();
            // If Position Input Is Out Of Range. ↓
            if (x_pos > 3 || y_pos > 3 || x_pos < 1 || y_pos < 1) {
                System.out.println(">>>Out Of Range<<<");
            } // If Position Already Taken. ↓
            else if (OXGame.getOX(x_pos - 1, y_pos - 1) != '-') {
                System.out.println(">>>Position Taken<<<");
            }// If Position Is Empty. Set The Position. ↓
            else {

                System.out.println("");
                OXGame.setTable(x_pos - 1, y_pos - 1, player);
                OXGame.callTable();
                round++;
            }
            // Check If There Is A Winner. ↓
            if (OXGame.checkIsWin(player) == true) {
                winner = player;
                break;
            }

        }
        // If Playing 9 Rounds And Still Not Found A Winner Yet.
        // The Game Can Be Considered A Draw. ↓
        System.out.println("");
        if (round == 9) {
            System.out.println(">>>Draw<<<");
        } // Call The Winner Side. ↓
        else {
            System.out.println(">>>" + winner + " Win<<<");
        }
    }

}
